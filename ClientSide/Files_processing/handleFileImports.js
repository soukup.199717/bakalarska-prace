/** let googleDocs = [];
 * JSON array, které obsahuje všechny Google Dokumenty, které se našly pro všechny právě zobrazené obrázky v menu
 *  content         {String}
 *  iconLink        {URL}
 *  id              {String}
 *  mimeType        {String}
 *  name            {String}
 *  parents         [array{String}]
 *  thumbnailLink   {URL}
 *  webViewLink     {URL}
 */
let googleDocs = [];
/**
 * Vytváří googleDocs JSON objekt, který se bude používat ve vyhledávání slov
 * @param {JSON Obj} file metadata o txt dokumentu
 */
function getGoogleDocContent(file) {
    var fileId = file.id;
    gapi.client.drive.files.export({
        fileId: fileId,
        mimeType: 'text/plain',
    }).then((res) => {
        var content = res.body;
        file.content = content;
        googleDocs.push(file);
    }, (err) => {
        console.log("An ERROR has occured in \'getGoogleDocContent\': ");
        console.error(err);
    });
}
/**
 * Vyhledávací alg: 1) Pokud není načtený žádný obrázek, nic víc mě nezajímá a alg končí. => podmínka v listFolders();
 *                  2) Společně s načítáním obrázku do menu se získají i metadata i obsah txt dokumentů
 *                  3) Tyto data se uloží do JSON objektu, který bude globální a z funkce ji vytáhnu díky asynch funkci a příkazu await
 *                  4) Dále už jen regex ...viz UI.js -- loadToSearchAll();
 */

 /**
  * Získává metadata o textových dokumentech spojenýh s obrázky, které jsou právě zobrazeny v menu.
  * Metoda připravuje JSON objekt, který bude využíván k vyhledávání klíčových slov.
  * @param {JSON Array} pics Pole obsahující jména načtených obrázků v menu
  */
function searchPrep(pics) {
    if(pics.length == 0) return;
    var query = "(";
    var name;
    pics.forEach(e => {
        if(e.name.split(".")[1] == e.name.split(".")[1].toLowerCase()) name = e.name.split(".")[0];
        else name = e.name;
        
        if(e !== pics[pics.length - 1]) {
            query += "name: \'" + name + "\' or ";
        }else {
            query += "name: \'" + name + "\') and mimeType = \"application/vnd.google-apps.document\" and trashed = false";
        }
    });
    gapi.client.drive.files.list({
        pageSize: 100,
        q: query,
        orderBy: "name",
        fields: 'files(id, name, mimeType, parents, iconLink, thumbnailLink, webViewLink)'
    }).then(function(response) {
        var files = response.result.files;
        if (files && files.length > 0) {
            files.forEach(e => {
                getGoogleDocContent(e);
            });
        }else console.log('Žádné textové dokumenty ve složce ');
    }, (err) => {
        console.log("An ERROR has occured in \'searchPrep\': ");
        console.error(err);
    });
}

/**
 * Načítá zvolený obrázek do aplikace
 * @param {String} fileId obrázku, který byl vybrán
 */
function getPicture(fileId) {
    gapi.client.drive.files.get({
        fileId: fileId,
        fields: '*'
    }).then((res) => {
        var jpg = res.result.webContentLink;
        document.getElementById("jpg").src = jpg;
    }, (err) => {
        console.log("An ERROR has occured in \'getPicture\': ");
        console.error(err)
    });
}

/**
 *
 * Vrací odkaz na Google dokument a vkládá ho do 'src' iframe 
 * @param {String} parentId id složky, ve které by se měl dokument nacházet
 * @param {String} fileName název obrázku i Google Doc musí být stejný (s vyjímkou koncovky samozdřejmně)
 */
function getGoogleDoc(parentId, fileName) {
    gapi.client.drive.files.list({
        pageSize: 1,
        q: "\'" + parentId + "\' in parents and name: \'" + fileName + "\' and trashed = false and mimeType = \"application/vnd.google-apps.document\"",
        fields: 'files(*)'
    }).then(function(response) {
        var files = response.result.files;
        if (files && files.length > 0) {
            document.getElementById('txt').style.display = 'block';
            if(document.getElementById("textNotFoundBox")) document.getElementById("textNotFoundBox").remove();
          document.getElementById('txt').src = files[0].webViewLink;
        }else {
            console.log('Looks like you don\'t have any text file for this picture yet');
            document.getElementById('txt').style.display = 'none';
            generateTranslateOCRMsg();
        }
    }, (err) => {
        console.log("An ERROR has occured in \'getGoogleDoc\': ");
        console.error(err.result.error.message)
        console.error("Probably not allowed characters in " + fileName)
    });
}

function generateTranslateOCRMsg() {
    textNotFoundBox = document.createElement("div");
    txtCoverDiv = document.createElement("div");
    txt404 = document.createElement('img');
    errMsg = document.createElement("p");
    createTxtFileBtn = document.createElement("button");
    btnDiv = document.createElement("div");
    loader = document.createElement("div");
    msg = document.createElement('p');
    txt404.src = '/miscs/text-file-not-found.png';
    textNotFoundBox.id = 'textNotFoundBox';
    loader.id = "loader";
    errMsg.id = "errMsg";
    btnDiv.id = "translateBtnLoader";
    errMsg.innerHTML = "Vypadá to, že k vybranému obrázku ještě nemáte vytvořený textový soubor"
    msg.innerHTML = "Přeloží obrázek do textového souboru"
    createTxtFileBtn.innerHTML = "Přeložit";
    createTxtFileBtn.title = "Vytvoří nový textový soubor a OCR přeloží obrázek"
    createTxtFileBtn.addEventListener("click", translateImgToTxt);
    txtCoverDiv.appendChild(errMsg)
    btnDiv.appendChild(createTxtFileBtn)
    btnDiv.appendChild(loader);
    textNotFoundBox.appendChild(txt404)
    textNotFoundBox.appendChild(txtCoverDiv)
    textNotFoundBox.appendChild(btnDiv)
    textNotFoundBox.appendChild(msg)
    document.getElementById('text').prepend(textNotFoundBox);
}

function getChosenFileId() {
    id = null;
    document.getElementById("fileMenu").childNodes.forEach(e => {
        if (e.children[0].lastChild.checked) {
            id = e.children[0].id
            return;
        }
    });
    return id;
}
function translateImgToTxt() {
    document.getElementById("loader").style.display = 'block'
    fileId = getChosenFileId();
    fileTitle = document.getElementById(fileId).children[0].id;
    var koncovka = fileTitle.split(".")[1];
    if(koncovka == fileTitle.split(".")[1].toLowerCase()) fileTitle = fileTitle.split(".")[0];
    const body = { 
        title: fileTitle,
        mimeType: 'application/vnd.google-apps.document',
    };
    gapi.client.drive.files.copy({
        fileId: fileId,
        resource: body
    }).then((res) => {
        gapi.client.drive.files.get({
            fileId: res.result.id,
            fields: 'webViewLink'
        }).then((res) => {
            document.getElementById("textNotFoundBox").remove();
            document.getElementById('txt').style.display = 'block';
            document.getElementById('txt').src = res.result.webViewLink;
            addTranslatedDocToSearch();
        }, (err) => {
            console.log("An ERROR has occured in \'translateImgToTxt[inner]\': ");
            console.error(err)
        });
    }, (err) => {
        console.log("An ERROR has occured in \'translateImgToTxt\': ");
        console.error(err)
        document.getElementById("errMsg").innerHTML = err.result.error.message
        document.getElementById("errMsg").style.color = "red";
        document.getElementById("loader").style.display = "none";
    });
}

function addTranslatedDocToSearch() {
    var fnames = []
    var arr = document.getElementById("fileMenu").children;
    for (let i = 0; i < arr.length; i++) {
        const e = arr[i];
        fnames.push({ name: e.title })
    }
    searchPrep(fnames);
}

/**
 * Vrací odkaz na Google dokument a vkládá ho do 'src' iframe.
 * Funkce je používána po kliknutí na element dokumentu ve vyhledávání.
 * @param {String} fileId id Google Dokumentu
 */
function getGoogleDoc_txt(fileId) {
    gapi.client.drive.files.get({
        fileId: fileId,
        fields: 'webViewLink'
    }).then((res) => {
        var txt = res.result.webViewLink;
        document.getElementById('txt').src = txt;
    }, (err) => {
        console.log("An ERROR has occured in \'getGoogleDoc_txt\': ");
        console.error(err)
    });
}

/**
 * Hlavní metoda celého načítání souborů. 
 * @param {*} pageToken určuje, zda je list konečný
 * @param {Boolean} includeShared zda si uživtel přeje listovat i mezi sdílenými soubory
 * @param {String} parentId obsah jaké složky se má zobrazit, pokud není specifikována žádná složka, bere se kořenová
 */
function listFolders(pageToken, includeShared, parentId) {
    document.getElementById("next100search").style.opacity = "0";
    document.getElementById("next100search").style.marginLeft = "-220px";
    if (!parentId) parentId = 'root';
    var mimeType = "(mimeType = 'application/vnd.google-apps.folder' or mimeType = 'image/bmp' or mimeType = 'image/x-portable-bitmap' or mimeType = 'image/png' or mimeType = 'image/jpeg')"
    var query = mimeType + " and trashed = false and ";

    if (includeShared) {
        if (parentId == 'root') query += "sharedWithMe";
        else query += "\'" + parentId + "\' in parents";
    }else query += "'me' in owners and \'" + parentId + "\' in parents";

    gapi.client.drive.files.list({
        pageSize: 100,
        q: query,
        pageToken: pageToken ? pageToken : '',
        orderBy: 'name',
        fields: 'nextPageToken, files(id, name, mimeType, parents, iconLink, thumbnailLink, webContentLink)'
    }).then(function(response) {
        var res = response.result.files;
        if (res && res.length > 0) {
            renderFolderlist(res, includeShared);
            howManyFiles();
            getNoFileMesage();
            if (response.result.nextPageToken) {
                listFolders(response.result.nextPageToken, includeShared, parentId);
            }
        } else {
            console.log('No files found.');
            howManyFiles();
            getNoFileMesage(parentId);
        }
    }, (err) => {
        console.log("An ERROR has occured in \'listFolders\': ");
        console.error(err)
    });
}
/**
 * Prostředník umožňující načíst obrázek i text do aplikace
 * @param {label HTML tag} label potomek Divu zastupující složku/soubor v seznamu
 * @param {String} name Název vybraného souboru
 */
function loadSelectedFiles(label, name){
    var newName;
    if(name.split(".")[1] == name.split(".")[1].toLowerCase()) newName = name.split(".")[0];
    else newName = name;
    getPicture(label.id);
    getGoogleDoc(label.parentElement.id, newName);
}

function loadSelectedFiles_search(label){
    var pics = document.getElementsByClassName("folderName");
    for (let i = 0; i < pics.length; i++) {
        const e = pics[i];
        if(e.firstChild.innerHTML.includes(label.firstChild.innerHTML)) {
            getGoogleDoc_txt(label.id);
            getPicture(e.id);
            e.lastChild.checked = true;
            e.parentElement.style.backgroundColor = "#d8f2fd48";
            e.parentElement.style.color = "blue";
            e.parentElement.style.boxShadow = "2px 2px 8px 1px #dadada";
            folderMenu(label.lastChild, e.lastChild);
            return;
        }
    }
  }
/**
 * Samostatná funkce umožňující pohyb zpět v menu. Id předešlé složky získává z div prvků v menu
 */
function moveBackinMenu() {
  var id;
  try {
    id = document.getElementsByClassName("divFolder")[0].id;
  } catch (TypeError) {
    id = document.getElementsByClassName("noFileMessage")[0].id;
  }
  gapi.client.drive.files.get({
    fileId: id,
    fields: 'id, parents'
}).then((res) => {
    deleteOldMenuElements(document.getElementById("folderMenu"));
    deleteOldMenuElements(document.getElementById("fileMenu"));
    googleDocs = [];
    if(!res.result.parents) {
      document.getElementById('goBackButton').disabled = true;
      listFolders('', document.getElementById("sharedFileSwitch").checked);
    } else {
    listFolders('', document.getElementById("sharedFileSwitch").checked, res.result.parents[0]);
    }
}, (err) => {
    console.log("An ERROR has occured in \'moveBackinMenu\': ");
    console.error(err)
});
}

/**
 * Vyhledávání v menu, načítá pouze prvních 100 výskytů, aby nedocházelo ke zpožděnému rendrování menu.
 * Proto pokud je více výskytů, objeví se tlačítko "další->", které obsahuje nextPageToken, který načte dalších <= 100 výskytů.
 * @param {*} pageToken určuje, zda je list konečný
 * @param {String} value input od uživatele ze search baru
 * @param {Boolean} includeShared zda si uživtel přeje listovat i mezi sdílenými soubory

 */
function searchInDrive(pageToken, value, includeShared) {
    var mimeType = "(mimeType = 'application/vnd.google-apps.folder' or mimeType = 'image/bmp' or mimeType = 'image/x-portable-bitmap' or mimeType = 'image/png' or mimeType = 'image/jpeg')"
    var query = mimeType + " and trashed = false and name contains \'" + value + "\'";
    if (!includeShared) query += " and 'me' in owners";

    gapi.client.drive.files.list({
        pageSize: 100,
        q: query,
        pageToken: pageToken ? pageToken : '',
        orderBy: 'name',
        fields: 'nextPageToken, files(id, name, mimeType, parents, iconLink, thumbnailLink, webContentLink)'
    }).then((response) => {
        var files = response.result.files;
        var goNextButton = document.getElementById("next100search");
        if (files && files.length > 0) {
            renderFolderlist(files, includeShared);
            howManyFiles();
            getNoFileMesage();
            if (response.result.nextPageToken) {
                console.log("There are more files..")
                goNextButton.name = response.result.nextPageToken;
                goNextButton.style.marginLeft = "220px";
                goNextButton.style.opacity = "1";
            }else {
                goNextButton.style.marginLeft = "-220px";
            }
        }else {
            deleteOldMenuElements(document.getElementById("folderMenu"));
            deleteOldMenuElements(document.getElementById("fileMenu"));
            howManyFiles();
            getNoFileMesage();
            console.log('No files found');
        }
    }), (err) => {
        console.log("An ERROR has occured in \'searchInDrive\': ");
        console.error(err)
    }
}