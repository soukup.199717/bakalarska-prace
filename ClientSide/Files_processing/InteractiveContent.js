const CHAR_TO_NUMBER = ['l', '1', 'O', '0', 'o', '0', 'S', '5', 'Z', '2'];

/**
 * Najde a opraví případné chyby v OCR překladu a následně vybere ze stránky dokumentu číslo, které by mohlo být číslo stránky
 * Spousta chyb - není 100% přesný - velice záleží na kvalitě OCR překladu
 * @returns Pole s názvem souboru a jeho číslem stránky
 */
 function getPageNumbers() {
    var contentParagraphs = [];
    var possiblePageNumbers;
    var listofPages = [];
    var chosenPage;
    var isEmpty;
    for (let i = 0; i < googleDocs.length; i++) {
        contentParagraphs = googleDocs[i].content.split("\n");
        possiblePageNumbers = [];
        k = 0;
        isEmpty = true;
        for (let j = 0; j < contentParagraphs.length; j++) {
            possiblePageNumbers[j] = null;
            if(contentParagraphs[j].length <= 6) {
                possiblePageNumbers[j] = adjustOCRmistakes(contentParagraphs[j]);
                if(possiblePageNumbers[j] !== null) isEmpty = false;
            }
        }
        if(isEmpty) possiblePageNumbers = null;
        chosenPage = editRegexToNum(chooseTheRightPageNumber(possiblePageNumbers));
        listofPages[i] = {
            fileName: googleDocs[i].name,
            page: chosenPage
        }
    }
    return listofPages;
}

/**
 * Vytváří jedno číslo ze pole čísel
 * @param {JSON Obj} digitRegex objekt obsahující index a regex.digitRegex a regex.nondigitRegex
 * @returns číslo složené z pole hodnot
 */
 function editRegexToNum(digitRegex) {
    if(!digitRegex) return null;
    var tmp, num = "";
    for (let i = 0; i < digitRegex.regex.digitRegex.length; i++) {
        if(digitRegex.regex.digitRegex[i] === null) continue;
        tmp = digitRegex.regex.digitRegex[i].match(/\d/);
        if(tmp !== null) {
            num += tmp;
        }
    }
    return num;
}

/**
 * Funkce využívá 3 priority:
 *  Pokud hodnota splňuje všechny tyto priority, je vybrána okamžitě. 
 *  Pokud více hodnot splňuje tyto priority, vybere se ta hodnota, jejíž index bude blíže 0 či n
 *  Pokud se nenajde hodnota, která splňuje všechny tyto priority, vybere se ta, která jich splňuje nejvíce. Zase, pokud se 
 *  najdou více hodnot splňující stejné priority, vybere se ta, jejíž index bude blíže k 0 či n.
 *  Pokud budou mít stejnou vzdálenost od 0 a n, tak se vybere ta blíže k 0
 * @param {Array<String>} possibility Pole s odstavci
 * @returns Vybraný prvek obsahující číslo stránky.
 */
 function chooseTheRightPageNumber(possibility) {
    if(possibility === null) return;
    var firstPriority;//největší priorita: čisté číslo - nondigitRegex === null || nondigitRegex === ""
    var secondPriority;//druhá priorita: umístění čísla je - (i <= 10) || (i >= possibility.length - 10) nebo-li (0 - 10) || (n-10 - n)
    var thirdPriority;//Třetí priorita: byla hodnota upravena v adjustOCRmistakes()? NE - vyhovuje, ANO - nevyhovuje
    var bestBet = [];
    var twoOutta3 = [];
    var oneOutta3 = [];
    var o = 0, p = 0, q = 0;

    for (let i = 0; i < possibility.length; i++) {
        if(possibility[i] === null) continue;
        const { digitRegex, nondigitRegex } = possibility[i];
        firstPriority = true;
        secondPriority = false;
        thirdPriority = true;
        canPass = false;
        if((nondigitRegex[0] === null || nondigitRegex[0] === ".") && (nondigitRegex[nondigitRegex.length-1] === null || nondigitRegex[nondigitRegex.length-1] === ".")) {
            canPass = true;
       }else firstPriority = false;
        for (let j = 0; j < nondigitRegex.length; j++) {
            if(digitRegex[j] != null && (nondigitRegex[j] != null && nondigitRegex[j] != ".")) thirdPriority = false;
            if(j > 0 && j < nondigitRegex.length - 1 && canPass) {
                if(nondigitRegex[j] !== null) firstPriority = false;
            }
        }
        if(i <= 10 || i >= possibility.length-10) secondPriority = true;
        
        if(!firstPriority && !secondPriority && !thirdPriority) continue;

    /* 1 1 1 */
        if(firstPriority && secondPriority && thirdPriority) { 
            bestBet[o] = {
                regex: possibility[i],
                index: i
            }
            o++;
        }
    /* 1 1 0 */
        else if(firstPriority && secondPriority && !thirdPriority) {
            twoOutta3[p] = {
                regex: possibility[i],
                index: i
            }
            p++;
        }
    /* 1 0 1 */
        else if(firstPriority && !secondPriority && thirdPriority) {
            twoOutta3[p] = {
                regex: possibility[i],
                index: i
            }
            p++;
        }
    /* 0 1 1 */
        else if(!firstPriority && secondPriority && thirdPriority) {
            twoOutta3[p] = {
                regex: possibility[i],
                index: i
            }
            p++;
        }
    /* 100  010  001*/
        else {
            oneOutta3[q] = {
                regex: possibility[i],
                index: i
            }
            q++;
        }
    }

    if(bestBet.length === 0) bestBet = null;
    if(twoOutta3.length === 0) twoOutta3 = null;
    if(oneOutta3.length === 0) oneOutta3 = null;
    return spitBestOption(possibility, bestBet, twoOutta3, oneOutta3);
}

/**
 * Vybírá tu nejvhodnější možnou cestu, jak nalézt číslo stránky.
 * @param {Array<String>} possibility Pole s odstavci
 * @param {Array<JSON>} bestBet Regex pole s prvky splňující všechny 3 priotrity
 * @param {Array<JSON>} twoOutta3 Regex pole s prvky splňující 2 ze 3 priorit
 * @param {Array<JSON>} oneOutta3 Regex pole s prvky splňující 1 ze 3 priorit
 * @returns Jeden prvek, který nejvíce vyhovuje všem kritériúm
 */
 function spitBestOption(possibility, bestBet, twoOutta3, oneOutta3) {
    if(bestBet === null) {
        if(twoOutta3 === null) {
            if(oneOutta3 === null) return null;
            if(oneOutta3.length === 1) return oneOutta3[0];
            return getCloserToLim(oneOutta3, possibility);
        }else{
            if(twoOutta3.length === 1) return twoOutta3[0];
            return getCloserToLim(twoOutta3, possibility);
        }
    }else {
        if(bestBet.length === 1) return bestBet[0];
        return getCloserToLim(bestBet, possibility);
    }
}

/**
 * Vyhodnocuje, který z výsledkú je blíže ke své limitě. 
 * Tato metoda se volá pouze pokud se vy výsledcích objeví několik hodnot splňující stejné priority.
 * @param {Array<JSON>} regexArray Pole s výsledky vyhledávání
 * @param {Array<String>} possibility Pole s odstavci
 * @returns Hodnotu, která je blíže ke své limitě
 */
 function getCloserToLim(regexArray, possibility) {
    var isClosertoTop, upperLim;
    var highTmp = { index: -999 };
    var lowTmp = { index: 999 };
    var distance;
    for (let i = 0; i < regexArray.length; i++) {
        upperLim = Math.abs(regexArray[i].index - possibility.length);
        if(upperLim > regexArray[i].index) isClosertoTop = false;
        else isClosertoTop = true;

        if(isClosertoTop) {
            if(highTmp.index < regexArray[i].index) {
                highTmp = regexArray[i];
            }
        }else {
            if(lowTmp.index > regexArray[i].index) {
                lowTmp = regexArray[i];
            }
        }
    }
    distance = Math.abs(highTmp.index - possibility.length);
    if(distance < lowTmp.index) {
        return highTmp;
    }else {
        return lowTmp;
    }
}

/**
 * Opravuje chyby při OCR překladu, kde jsou čísla podobná písmenum.
 * V nondigitRegex je pole všech písmen z vybraného řádku, pokud je v řádku alespoň jedno číslo, pokračuje se dále do for cyklu.
 * Zde se po jednotlivých charech vyhledávají písmena podobná číslum a přepíší se do digitRegex
 * @param {String} paragraph Řádek ze souboru
 */
function adjustOCRmistakes(paragraph) {
    const {digitRegex, nondigitRegex} = matchRegexesToFindPageNum(paragraph);
    if(hasParagraphDigit(digitRegex)) {
        for (let i = 0; i < nondigitRegex.length; i++) {
            if(nondigitRegex[i] === null) continue;//přeskočí jednu iteraci
            switch (nondigitRegex[i]) {
                case CHAR_TO_NUMBER[0]:
                    digitRegex[i] = CHAR_TO_NUMBER[1];
                    console.log("l to 1");
                    break;
                case CHAR_TO_NUMBER[2]:
                    digitRegex[i] = CHAR_TO_NUMBER[3];
                    console.log("O to 0");
                    break;
                case CHAR_TO_NUMBER[4]:
                    digitRegex[i] = CHAR_TO_NUMBER[5];
                    console.log("O to 0");
                    break;
                case CHAR_TO_NUMBER[6]:
                    digitRegex[i] = CHAR_TO_NUMBER[7];
                    console.log("S to 5");
                    break;
                case CHAR_TO_NUMBER[8]:
                    digitRegex[i] = CHAR_TO_NUMBER[9];
                    console.log("Z to 2");
                    break;
                default:
                    if(nondigitRegex[0] !== null) nondigitRegex[0] = ".";
                    if(nondigitRegex[nondigitRegex.length-1] !== null) nondigitRegex[nondigitRegex.length-1] = ".";
                    break;
            }
        }

        return { digitRegex:digitRegex, nondigitRegex: nondigitRegex };
    }
    return null;
}

/**
 * Funkce prohledává 1 řádek dokumentu a jeho obsah rozděluje na číselnou část a nečíselnou.
 * digitRegex je pole, které se naplní hodnotami z regulérních výrazu, které vyhledávají pouze čísla
 * nondigitRegex je pole které se naplní nečíselnými hodnoty
 * @param {String} fullParagraph String, jeden řádek z dokumentu
 */
 function matchRegexesToFindPageNum(fullParagraph) {
    var digitRegex = [], nondigitRegex = [];
    for (let i = 0; i < fullParagraph.length; i++) {
        digitRegex[i] = fullParagraph[i].match(/\d/);
        nondigitRegex[i] = fullParagraph[i].match(/\D/);
        if(digitRegex[i] !== null) digitRegex[i] = digitRegex[i][0];
        if(nondigitRegex[i] !== null) nondigitRegex[i] = nondigitRegex[i][0];
    }
    return { digitRegex: digitRegex, nondigitRegex: nondigitRegex };
}

/**
 * Zkoumá zda řádek obsahuje číslo
 * @param {Array<RegExp>} digitRegex řádek dokumentu zbaven všech nečíslic
 */
function hasParagraphDigit(digitRegex) {
    for (let i = 0; i < digitRegex.length; i++) {
        if(digitRegex[i] != null) return true;
    }
    return false;
}

function getDocFileFromGoogleDoc(fname) {
    for (let i = 0; i < googleDocs.length; i++) {
        if(googleDocs[i].name === fname) return googleDocs[i];
    }
}

/**
 * Spojuje @contentTable s @pages podle čísla stránek
 * @param {Array<JSON>} contentTable obsah stránky s obsahem knihy
 * @param {Array<JSON>} pages nazev a číslo stránky souboru
 * @returns výsledné spojené pole
 */
function joinContentToPages(contentTable, pages) {
    var unionArray = [];
    for (let i = 0; i < contentTable.length; i++) {
        unionArray[i] = {
            fileName: "404",
            page: contentTable[i].page,
            title: contentTable[i].title
        }
        for (let j = 0; j < pages.length; j++) {
            if(contentTable[i].page === pages[j].page) {
                unionArray[i] = {
                    fileName: pages[j].fileName,
                    page: pages[j].page,
                    title: contentTable[i].title
                }
                break;
            }
        }
    }
    return unionArray;
}

/*
<div>                               .divFolder-content
	<label>                         .folderName-content
		<span>[pageNum]</span>
		<div>
			<span>[title]</span>
			<br>
			<span>[fileName]</span>
		</div>
		<img>                       .folderIcon
		<input type="radio">        .root
	</label>
</div>
*/
function renderTableOfContent(contentTable, pages) {
    let radio, img, label, span, div, innerDiv, innerSpan1, innerSpan2, br, menuFile, coverDiv;
    coverDiv = document.createElement('div')
    coverDiv.id = 'coverDiv-content'
    var joinedArray = joinContentToPages(contentTable.paragraphs, pages);
    var menuFiles = document.getElementsByClassName("divFolder");
    for (let i = 0; i < joinedArray.length; i++) {
        for (let j = 0; j < menuFiles.length; j++) {
            if(joinedArray[i].fileName === menuFiles[j].title.split('.')[0]) {
                joinedArray[i].iconLink = menuFiles[j].children[0].children[1].src;
                joinedArray[i].picId = menuFiles[j].children[0].id;
                break;
            }
        }
    }
    console.log(joinedArray);

    joinedArray.forEach(e => {
        
      div = document.createElement("div");
      div.className = "divFolder-content";
      div.title = e.title;
      div.addEventListener("mouseover", function() {
            this.style.boxShadow = "2px 2px 8px 1px #dadada";
        });
        div.addEventListener("mouseout", function() {
            if(this.children[0].children[3].checked) return;
            this.style.boxShadow = "none";
        });
      label = document.createElement("label");
      label.className = "folderName-content";
      label.id = e.picId;
      span = document.createElement("span");
      span.innerHTML = e.page;
      span.id = e.fileName;

      menuFile = getSameFileFromMainMenu(e.picId);
      if(menuFile === null) div.id = '404'
      else div.id = menuFile.id;

      innerDiv = document.createElement('div');
      innerSpan1 = document.createElement('span');
      br = document.createElement('br');
      innerSpan2 = document.createElement('span');
      innerSpan1.innerHTML = e.title;

      img = document.createElement("img");
      img.className = "folderIcon";
      
      if(e.fileName === "404") {
        innerSpan2.innerHTML = "Nenalezeno";
        innerSpan2.style.color = 'red';
        img.src = "../miscs/not-found.png"
      }else {
        innerSpan2.innerHTML = e.fileName;
        img.src = e.iconLink;
    }

      radio = document.createElement("input");
      radio.type = "radio";
      radio.className = 'root';
      radio.name = 'file-content';

      radio.addEventListener("change", function() {
        fileSelectEvent(this, getSameFileFromMainMenu(this.parentElement.id));
      });
      
      innerDiv.appendChild(innerSpan1);
      innerDiv.appendChild(br);
      innerDiv.appendChild(innerSpan2);
      label.appendChild(span);
      label.appendChild(innerDiv);
      label.appendChild(img);
      label.appendChild(radio);
      div.appendChild(label);
      
      coverDiv.appendChild(div);
    });
    document.getElementById("tableOfContent").appendChild(coverDiv)
}

function getSameFileFromMainMenu(id) {
    var menu = document.getElementById("fileMenu").children;
    for (let i = 0; i < menu.length; i++) {
        if(menu[i].children[0].id === id) return menu[i];
    }
    return null;
}