function findWords(keyWord, file) {
    var content = file.content;
    var re = new RegExp(keyWord, 'gi');
    if(!content.match(re)) return null;
    var occurance = content.match(re).length;
    var searchResult = [];
    searchResult["keyword"] = keyWord;
    searchResult["occurance"] = occurance;
    file.searchResult = searchResult;
    return file;
}

function wordOccurances(keyWord, googleDocs) {
    var newGoogleDocs = [];
    var totalOccurance = 0;
    var file;
    for (let i = 0; i < googleDocs.length; i++) {
        file = findWords(keyWord, googleDocs[i]);
        if(file) {
            totalOccurance += file.searchResult.occurance;
            newGoogleDocs.push(file);
        }
    }
    newGoogleDocs.sort((a,b) => {return b.searchResult.occurance - a.searchResult.occurance});
    console.log(newGoogleDocs);
    console.log("total occurance: " + totalOccurance);
    renderSearchResults(newGoogleDocs);
    const searchHeader = document.getElementById("searchResultHeader");
    var totalOcc = document.createElement("h2");
    totalOcc.className = "headerResult";
    totalOcc.id = "totalOccurance";
    totalOcc.innerHTML = "Celkem <span>" + totalOccurance + "</span> výskytů";
    searchHeader.appendChild(totalOcc);
}


function renderSearchResults(newGoogleDocs) {
    var div, label, span, span2, radio, img, br, tmp;
    var renderCanvas = document.getElementById("searchResults");
    newGoogleDocs.forEach(e => {
        tmp = "shod";
        div = document.createElement("div");
        div.className = "divFolder-search";
        if(e.parents) div.id = e.parents[0];
        div.title = e.name;
        div.addEventListener("mouseover", function() {
            this.style.boxShadow = "2px 2px 8px 1px #dadada";
        });
        div.addEventListener("mouseout", function() {
            if(this.children[0].children[4].checked) return;
            this.style.boxShadow = "none";
        });

        label = document.createElement("label");
        label.className = "folderName-search";
        label.id = e.id;

        span = document.createElement("span");
        span.innerHTML = e.name;
        span.id = e.name;

        br = document.createElement("br");

        span2 = document.createElement("span");
        if(e.searchResult.occurance == 1) tmp = "shoda"
        if(e.searchResult.occurance > 1 && e.searchResult.occurance < 5) tmp = "shody"
        span2.innerHTML = "<b>" + e.searchResult.occurance + "</b> " + tmp;

        img = document.createElement("img");
        img.className = "folderIcon";
        img.src = e.iconLink;

        radio = document.createElement("input");
        radio.type = "radio";
        radio.name = 'file-search';
        radio.className = 'root';

        radio.addEventListener("change", function () {
            var parent = this.parentElement.parentElement;
            if (this.checked) {
                parent.style.backgroundColor = "#d8f2fd48";
                parent.style.color = "blue";
                parent.style.boxShadow = "2px 2px 8px 1px #dadada";
                loadSelectedFiles_search(this.parentElement);
                hideSearch(false);
            }
        });

        label.appendChild(span);
        label.appendChild(br);
        label.appendChild(span2);
        label.appendChild(img);
        label.appendChild(radio);
        div.appendChild(label);
        renderCanvas.appendChild(div);
    });
    var nResultsInFolder = document.createElement("h2");
    nResultsInFolder.id = 'nResultsInFolder';
    nResultsInFolder.className = "headerResult";
    nResultsInFolder.innerHTML = "Nalezeno <span>" + newGoogleDocs.length + "</span> z celkových <span>" + googleDocs.length + "</span> souborů";
    document.getElementById("searchResultHeader").appendChild(nResultsInFolder);
}