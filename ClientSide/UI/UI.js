var handler = document.getElementById('separator');
var wrapper = handler.closest('.wrapper');
var boxA = wrapper.querySelector('.box');
var isHandlerDragging = false;
let zoom = 3;

//-------------Přihlašování-------------------------------------------------------------------------------------------------------------------
function hideWelcomeLayer() {
    welcomelayer = document.getElementById("welcomeLayer");
    welcomelayer.style.opacity = "1";
    welcomelayer.children[0].style.opacity = "1"
    setTimeout(() => {
        welcomelayer.style.opacity = "0";
    }, 6000)
    setTimeout(() => {
        welcomelayer.style.display = "none";
    }, 9000)
}

//--------------Menu-----------------------------------------------------------------------------------------------------------------------------
/**
 * Hlavní metoda obstarávající formátová dat do menu
 * @param {JSON Array} files metadata získaná ze složky google drivu
 */
function renderFolderlist(files) {
    let radio, img, label, span, div;
    let metadata = [];
    files.forEach(e => {
      div = document.createElement("div");
      div.className = "divFolder";
      if(e.parents) div.id = e.parents[0];
      div.title = e.name;
      div.addEventListener("mouseover", function() {
            this.style.boxShadow = "2px 2px 8px 1px #dadada";
        });
        div.addEventListener("mouseout", function() {
            if(this.children[0].children[2].checked) return;
            this.style.boxShadow = "none";
        });
      label = document.createElement("label");
      label.className = "folderName";
      label.id = e.id;
      span = document.createElement("span");
      span.innerHTML = e.name;
      span.id = e.name;
  
      radio = document.createElement("input");
      radio.type = "radio";
      radio.className = 'root';
      img = document.createElement("img");
      img.className = "folderIcon";
      if (e.mimeType === 'application/vnd.google-apps.folder') {
        radio.name = "folder";
        img.src = e.iconLink;
      } else {
        radio.name = 'file';
        img.src = e.thumbnailLink;
        metadata.push(e);
      }
      radio.addEventListener("change", function() {
        fileSelectEvent(this);
      });
      
      label.appendChild(span);
      label.appendChild(img);
      label.appendChild(radio);
      div.appendChild(label);
      if (e.mimeType === "application/vnd.google-apps.folder")
        document.getElementById('folderMenu').appendChild(div);
      else
        document.getElementById('fileMenu').appendChild(div);
    });
    searchPrep(metadata);
}
function fileSelectEvent(radio, parent2) {
    document.getElementById('goBackButton').disabled = false;
    var parent = radio.parentElement.parentElement;
    if (radio.checked) {
      parent.style.backgroundColor = "#d8f2fd48";
      parent.style.color = "blue";
      parent.style.boxShadow = "2px 2px 8px 1px #dadada";
      if(parent2) {
        parent2.style.backgroundColor = "#d8f2fd48";
        parent2.style.color = "blue";
        parent2.style.boxShadow = "2px 2px 8px 1px #dadada";
        parent2.children[0].lastChild.checked = true;
      }
      if (radio.name === 'folder') {
        googleDocs = [];
        deleteOldMenuElements(document.getElementById("folderMenu"));
        deleteOldMenuElements(document.getElementById("fileMenu"));
        deleteOldMenuElements(document.getElementById("searchResults"));
        deleteOldMenuElements(document.getElementById("searchResultHeader"));
        listFolders('', document.getElementById("sharedFileSwitch").checked, radio.parentElement.id);
        document.getElementById('showContent').style.display = 'none';
      } else {
        loadSelectedFiles(radio.parentElement, radio.parentElement.firstChild.id);
        document.getElementById("menu").style.display = 'none';
        document.getElementById("tableOfContent").style.display = 'none';
      }
    }
    if(parent2) folderMenu(radio, parent2.children[0].lastChild);
    else folderMenu(radio)
  }
/**
 * zobrazuje a schovává menu
 */
var menu = document.getElementById("menu");
var menuOpener = document.getElementById("browseFiles");
var tableOfContent = document.getElementById("tableOfContent");
menuOpener.addEventListener('click', function() {
    if(menu.style.display === 'block') {
        menu.style.display = 'none';
        tableOfContent.style.display = 'none';
    }
    else {
        menu.style.display = 'block';
        tableOfContent.style.display = 'block';
    }
});

document.addEventListener('mouseup', function(e) {
    document.getElementById("rightClickMenu").style.display = 'none';
    if (!menu.contains(e.target) && !menuOpener.contains(e.target) && !tableOfContent.contains(e.target) && !document.getElementById("setTableOfContent").contains(e.target)) {
      menu.style.display = 'none';
      tableOfContent.style.display = 'none';
    }
    if (!document.getElementById("searchResultCoverDiv").contains(e.target) && 
        !document.getElementById("searchInTheTexts-coverDiv").contains(e.target) && isOpen) {
        isOpen = !isOpen;
        hideSearch(isOpen);
    }
    if(!document.getElementById("userImg").contains(e.target) && !document.getElementById("settings").contains(e.target)
        && document.getElementById("settings").style.display == "block") {
            document.getElementById("settings").style.display = 'none';
        }
  });
/**
 * Informuje o prázdné složce
 * @param {String} parentId id předchozí složky, využíváno funkcí pro procházení zpět v menu
 */
function getNoFileMesage(parentId) {
    if(!document.getElementById("folderMenu").hasChildNodes()) {
        var message = document.createElement('h3');
        message.innerHTML = "Nemáte zde žádné složky";
        message.className = "noFileMessage";
        message.id = parentId;
        document.getElementById("folderMenu").appendChild(message);
    }
    if(!document.getElementById("fileMenu").hasChildNodes()) {
        var message = document.createElement('h3');
        message.innerHTML = "Nemáte zde žádné soubory";
        message.className = "noFileMessage";
        message.id = parentId;
        var span = document.createElement('i');
        span.innerHTML = "Podporované jsou pouze soubory s koncovkou .bmp .jpg .png .pbm"
        message.appendChild(span);
        document.getElementById("fileMenu").appendChild(message);
    }
}
/**
 * Resetuje styl ostatních prvků v seznamu v menu, které nebyly vybrány
 * @param {input[type="radio"]} elm prvek, který byl označen 
 */
function folderMenu(elm, elmSearch) {
    var radios = document.getElementsByClassName("root");
    var parent;
    for (let i = 0; i < radios.length; i++) {
        if (radios[i] != elm && radios[i] != elmSearch) {
            parent = radios[i].parentElement.parentElement;
            parent.style.backgroundColor = "transparent";
            parent.style.color = "rgb(87, 87, 87)"
            parent.style.boxShadow = 'none'
        }
    }
}
/**
 * Resetuje seznam v menu. Volá se vždy při změně složky
 */
function deleteOldMenuElements(htmlElement) {
    while (htmlElement.hasChildNodes()) {
        htmlElement.removeChild(htmlElement.lastChild);
    }
}
document.getElementById("goBackButton").addEventListener('click' , function() {    
    moveBackinMenu();
    if(isOpen_content) document.getElementById('showContent').click();
    document.getElementById('showContent').style.display = 'none';
});
/**
 * Počítá kolik složek a souborů je právě zobrazeno ve složce
 */
function howManyFiles() {
    var folders = document.getElementById('folderMenu').children;
    var files = document.getElementById('fileMenu').children;
    var heading = document.getElementsByClassName("menuHeading");
    heading[0].removeChild(heading[0].lastChild);
    heading[1].removeChild(heading[1].lastChild);
    span = document.createElement('span');
    if(folders.length == 1 && folders[0].className == "noFileMessage") {
        span.innerHTML = "Nalezeno 0 složek";
    }else span.innerHTML = "Nalezeno " + folders.length + " složek";
    heading[0].appendChild(span);
    span2 = document.createElement('span');
    if(files.length == 1 && files[0].className == "noFileMessage") {
        span2.innerHTML = "Nalezeno 0 souborů";
    }else span2.innerHTML = "Nalezeno " + files.length + " souborů";
    heading[1].appendChild(span2);
}

document.getElementById("searchFiles").addEventListener("change", function() {
    googleDocs = [];
    if(this.value === '') {
        deleteOldMenuElements(document.getElementById("folderMenu"));
        deleteOldMenuElements(document.getElementById("fileMenu"));
        listFolders("", document.getElementById("sharedFileSwitch").checked);
    }else  {
        deleteOldMenuElements(document.getElementById("folderMenu"));
        deleteOldMenuElements(document.getElementById("fileMenu"));
        searchInDrive('', this.value, document.getElementById("sharedFileSwitch").checked);
    }
});

document.getElementsByClassName("fa fa-search")[0].parentElement.addEventListener("click", function() {
    document.getElementById("searchFiles").onchange;
});

document.getElementById("next100search").addEventListener("click", function() {
    var value = document.getElementById("searchFiles").value;
    searchInDrive(this.name, value, document.getElementById("sharedFileSwitch").checked)
});


//--------------Nastaveni-------------------------------------------------------------------------------------------------------------------------
var isLoggingOut;
document.getElementById("userImg").addEventListener("click", function() {
    settingsBox = document.getElementById("settings");
    if(settingsBox.style.display == "block") settingsBox.style.display = 'none'; 
    else settingsBox.style.display = "block";

});
document.getElementById("sharedFileSwitch").addEventListener("change", () => {
    spans = document.getElementsByClassName("sharedFileSwitchText");
    if (document.getElementById("sharedFileSwitch").checked) {
        spans[0].style.color = "black"
        spans[0].style.textShadow = "none"
        spans[1].style.color = "red"
        spans[1].style.textShadow = "0px 0px 10px #ff7676"
    }else {
        spans[0].style.color = "red"
        spans[0].style.textShadow = "0px 0px 10px #ff7676"
        spans[1].style.color = "black"
        spans[1].style.textShadow = "none"
    }
    deleteOldMenuElements(document.getElementById("folderMenu"));
    deleteOldMenuElements(document.getElementById("fileMenu"));
    listFolders('', document.getElementById("sharedFileSwitch").checked);
});

document.getElementById("logout").addEventListener("click", () => {
    isLoggingOut = true;
    handleSignoutClick();
});

//--------------Interaktivní obsah----------------------------------------------------------------------------------------------------------------

var CONTENT_PAGE_NUMBER_IDENTIFIER = ["#"," "]; // číslo stránky bude ve stránce s obsahem značeno: # + [číslo] + mezera
var isOpen_content = false;
var rotateArrowBtn = 0;
document.getElementById("showContent").addEventListener("click", function() {
    isOpen_content = !isOpen_content;
    rotateArrowBtn = rotateArrowBtn+180;
    var tableOfContent = document.getElementById("tableOfContent");
    if(!isOpen_content) {
        tableOfContent.style.left = "100px"
        this.children[1].style.transform = 'rotate(' + rotateArrowBtn + 'deg)';
    }
    else {
        tableOfContent.style.left = "400px"
        this.children[1].style.transform = 'rotate(' + rotateArrowBtn + 'deg)';
    }
    if(rotateArrowBtn > 900) rotateArrowBtn = 0;
});

document.addEventListener("contextmenu", function(e) {
    var arr = document.getElementsByClassName("divFolder");
    var selectedNode = undefined;
    var end = true;
    for (let i = 0; i < arr.length; i++) {
        if(arr[i].contains(e.target) && !document.getElementById("folderMenu").contains(e.target)) {
            end = false;
            selectedNode = arr[i];
            break;
        }
    }
    if(end) return;
    e.preventDefault();
    var cover = document.getElementById("rightClickMenu");
    cover.style.top = e.pageY + "px";
    cover.style.left = e.pageX + "px";
    cover.style.display = "block";
    document.getElementById("select").addEventListener("mouseup", function() {
        var radio = selectedNode.children[0].children[2];
        radio.checked = true;
        fileSelectEvent(radio);
        selectedNode = undefined;
    });
    document.getElementById("setTableOfContent").addEventListener("mouseup", function() {
        var re = new RegExp(CONTENT_PAGE_NUMBER_IDENTIFIER[0] + "\+\\d\+" + CONTENT_PAGE_NUMBER_IDENTIFIER[1],'g');
        var selectedFile = selectedNode.title.split(".")[0];
        var file = getDocFileFromGoogleDoc(selectedFile);
        var docParagraphs = file.content.split("\n");
        var tableOfContent = [];
        tableOfContent.paragraphs = [];
        tableOfContent.fileName = selectedFile;
        for (let i = 0; i < docParagraphs.length; i++) {
            var pageNum = docParagraphs[i].match(re);// /#+\d+ /
            var tmp = "";
            if(pageNum !== "" && pageNum !== null) {
                for (let j = 0; j < pageNum.length; j++) {
                    tmp += pageNum[j] + "";
                }
                tmp = tmp.replace(CONTENT_PAGE_NUMBER_IDENTIFIER[0], "");
                tmp = tmp.replace(CONTENT_PAGE_NUMBER_IDENTIFIER[1], "");
                tableOfContent.paragraphs.push({page: tmp, title: docParagraphs[i].split(pageNum)[1]});
            }
        }
        selectedNode = undefined;
        deleteOldMenuElements(document.getElementById("tableOfContent"));
        fillOverview(selectedFile)
        renderTableOfContent(tableOfContent, getPageNumbers())
        document.getElementById('showContent').style.display = 'block';
        if(!isOpen_content) document.getElementById('showContent').click();
    });
});

function fillOverview(contentFile) {
    const tableOfContent = document.getElementById("tableOfContent")
    var header, name, text, infrontPage, behindPage, br, save, div, txt;
    header = document.createElement("div");
    name = document.createElement("h3");
    text = document.createElement('span')
    br = document.createElement('br')
    div = document.createElement('div')
    infrontPage = document.createElement("input");
    txt = document.createElement('span')
    behindPage = document.createElement("input");
    save = document.createElement('button')
    header.id = "contentOverview";
    infrontPage.id = 'infrontPageId'
    txt.innerHTML = "[číslo]"
    behindPage.id = 'behindPageId'
    save.id = "saveNewPageId";
    name.innerHTML = contentFile; 
    text.innerHTML = "Znaky k rozpoznání odstavců: "
    save.innerHTML = "uložit";
    infrontPage.type = 'text';
    behindPage.type = 'text';
    infrontPage.value = CONTENT_PAGE_NUMBER_IDENTIFIER[0]
    behindPage.value = CONTENT_PAGE_NUMBER_IDENTIFIER[1]

    save.addEventListener('click', function() {
        CONTENT_PAGE_NUMBER_IDENTIFIER[0] = document.getElementById('infrontPageId').value
        CONTENT_PAGE_NUMBER_IDENTIFIER[1] = document.getElementById('behindPageId').value
        this.innerHTML = "uloženo!"
        this.style.color = 'red';
        setTimeout(() => {
            document.getElementById('saveNewPageId').innerHTML = "uložit"
            document.getElementById('saveNewPageId').style.color = 'inherit';
        }, 1000);
    });
    

    div.appendChild(infrontPage)
    div.appendChild(txt)
    div.appendChild(behindPage)
    div.appendChild(save)

    header.appendChild(name);
    header.appendChild(text)
    header.appendChild(br);
    header.appendChild(div);

    tableOfContent.appendChild(header);
}

//---------------Vyhledavaci algoritmus-------------------------------------------------------------------------------------
const searchIcon = "https://img.icons8.com/cute-clipart/64/000000/search.png";
const closeIcon = "https://img.icons8.com/windows/32/000000/macos-close.png";
const icon = document.getElementById("bundle-icon");
const searchBox = document.getElementById("searchResultCoverDiv");
const coverDiv = document.getElementById("searchInTheTexts-coverDiv");
const searchBar = document.getElementById("searchText");
var isOpen = false;
document.getElementById("searchInTheTexts-coverDiv").addEventListener("mouseup", function() {
    isOpen = !isOpen;
    hideSearch(isOpen);
});

function hideSearch(isOpen) {
    if(isOpen){
        icon.src = closeIcon;
        coverDiv.style.backgroundColor = "transparent";
        searchBox.style.right = "20px";
        searchBox.style.opacity = "1";
    }else {
        icon.src = searchIcon;
        coverDiv.style.backgroundColor = "gray";
        searchBox.style.opacity = "0";
        searchBox.style.right = "-600px";
    }
}
searchBar.addEventListener("input", function() {
    deleteOldMenuElements(document.getElementById("searchResults"));
    deleteOldMenuElements(document.getElementById("searchResultHeader"));
    if(this.value == '') return;
    wordOccurances(this.value, googleDocs);
});

//--------------Posuvny prvek mezi obrazkem a textem---------------------------------------------------------------------------------------
var blanket = document.getElementById("blanket");
document.addEventListener('mousedown', function(e) {
    // If mousedown event is fired from .handler, toggle flag to true
    if (e.target === handler) {
        isHandlerDragging = true;
        blanket.style.display = "block";
    }
});

document.addEventListener('mousemove', function(e) {
    // Don't do anything if dragging flag is false
    if (!isHandlerDragging) {
        document.body.style.cursor = "auto";
        return false;
    }
    document.body.style.cursor = "w-resize";

    // Get offset
    var containerOffsetLeft = wrapper.offsetLeft;

    // Get x-coordinate of pointer relative to container
    var pointerRelativeXpos = e.clientX - containerOffsetLeft;

    // Arbitrary minimum width set on box A, otherwise its inner content will collapse to width of 0
    var boxAminWidth = 0;
    // Resize box A
    // * 8px is the left/right spacing between .handler and its inner pseudo-element
    // * Set flex-grow to 0 to prevent it from growing
    boxA.style.width = (Math.max(boxAminWidth, pointerRelativeXpos - 8)) + 'px';
    boxA.style.flexGrow = 0;
});

//--------------Lupa--------------------------------------------------------------------------------------------------------------------------

document.addEventListener('mouseup', function(e) {
    // Turn off dragging flag when user mouse is up
    isHandlerDragging = false;
    blanket.style.display = "none";
    if (document.getElementById("img-magnifier-glass") != null) {
        magnify(zoom);
    }
});
document.getElementById("picture").addEventListener("mouseup", function(e) {
    let magnifier = document.getElementById("magnify");
    if(!magnifier.checked)  {
        magnifier.checked = true;
        magnifier.onchange();
        return;
    }
    magnifier.checked = false;
    magnifier.onchange();
});
let magnifier = document.getElementById("magnify");
magnifier.onchange = function() {
    if (this.checked) {
        zoom = 3;
        magnify(zoom);
    } else {
        var child = document.getElementById("img-magnifier-glass");
        var parent = child.parentElement;
        parent.removeChild(child);
    }
};

document.getElementById("zoom-in").onclick = function() {
    if (magnifier.checked && zoom < 10) {
        zoom = zoom + 1;
        magnify(zoom);
    }
};
document.getElementById("zoom-out").onclick = function() {
    if (magnifier.checked && zoom > 1) {
        zoom = zoom - 1;
        magnify(zoom);
    }
};

function magnify(zoom) {
    var glass, w, h, bw;
    let img = document.getElementById("jpg");
    /*create magnifier glass:*/
    glass = document.getElementById("img-magnifier-glass");
    if (glass == null) {
        glass = document.createElement("DIV");
        glass.setAttribute("id", "img-magnifier-glass");
        /*insert magnifier glass:*/
        img.parentElement.insertBefore(glass, img);
        /*set background properties for the magnifier glass:*/
    }
    glass.style.backgroundImage = "url('" + img.src + "')";
    glass.style.backgroundRepeat = "no-repeat";
    glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
    bw = 3;
    w = glass.offsetWidth / 2;
    h = glass.offsetHeight / 2;
    /*execute a function when someone moves the magnifier glass over the image:*/
    glass.addEventListener("mousemove", moveMagnifier);
    img.addEventListener("mousemove", moveMagnifier);
    /*and also for touch screens:*/
    glass.addEventListener("touchmove", moveMagnifier);
    img.addEventListener("touchmove", moveMagnifier);

    function moveMagnifier(e) {
        var pos, x, y;
        /*prevent any other actions that may occur when moving over the image*/
        e.preventDefault();
        /*get the cursor's x and y positions:*/
        pos = getCursorPos(e);
        x = pos.x;
        y = pos.y;
        /*prevent the magnifier glass from being positioned outside the image:*/
        if (x > img.width - (w / zoom)) { x = img.width - (w / zoom); }
        if (x < w / zoom) { x = w / zoom; }
        if (y > img.height - (h / zoom)) { y = img.height - (h / zoom); }
        if (y < h / zoom) { y = h / zoom; }
        /*set the position of the magnifier glass:*/
        glass.style.left = (x - w) + "px";
        glass.style.top = (y - h) + "px";
        /*display what the magnifier glass "sees":*/
        glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
    }

    function getCursorPos(e) {
        var a, x = 0,
            y = 0;
        e = e || window.event;
        /*get the x and y positions of the image:*/
        a = img.getBoundingClientRect();
        /*calculate the cursor's x and y coordinates, relative to the image:*/
        x = e.pageX - a.left;
        y = e.pageY - a.top;
        /*consider any page scrolling:*/
        x = x - window.pageXOffset;
        y = y - window.pageYOffset;
        return { x: x, y: y };
    }
};

function removeMagnifier() {
    let zoom = document.getElementById("magnify");
    if (!zoom.checked) return;
    document.getElementById("magnify").checked = false;
    var child = document.getElementById("img-magnifier-glass");
    var parent = child.parentElement;
    parent.removeChild(child);
}